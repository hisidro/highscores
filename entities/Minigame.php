<?php

namespace entities;

/**
 * @Entity
 * @Table(name="minigames")
 */

 class Minigame {

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @OneToMany(targetEntity="Highscore", mappedBy="gameid")
     */
    private $id;

    /**
     * @Column(type="string")
     */
    private $name;

 
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Minigame
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
