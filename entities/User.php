<?php

namespace entities;

/**
 * @Entity
 * @Table(name="users")
 */

 class user {

     /**
      * @Id
      * @GeneratedValue
      * @Column(type="integer")
      */
    private $id;

    /**
     *  @Column(type="string") 
     */
    private $uname;

    /**
     *  @Column(type="string")
     */
    private $email;

    /**
     *  @Column(type="string")
     */
    private $pword;

 
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uname.
     *
     * @param string $uname
     *
     * @return user
     */
    public function setUname($uname)
    {
        $this->uname = $uname;

        return $this;
    }

    /**
     * Get uname.
     *
     * @return string
     */
    public function getUname()
    {
        return $this->uname;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return user
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set pword.
     *
     * @param string $pword
     *
     * @return user
     */
    public function setPword($pword)
    {
        $this->pword = $pword;

        return $this;
    }

    /**
     * Get pword.
     *
     * @return string
     */
    public function getPword()
    {
        return $this->pword;
    }
}
