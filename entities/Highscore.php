<?php

namespace entities;

/**
 * @Entity
 * @Table(name="high_scores")
 */

class Highscore {

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    private $id;

    /**
     * @Column(type="integer")
     * @ManyToOne(targetEntity="Minigame", inversedBy="id")
     * @JoinColumn(name="gameid", referencedColumnName="id")
     */
    private $gameid;

    /**
     * @Column(type="integer")
     */
    private $userid;

    /**
     * @Column(type="integer")
     */
    private $score;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gameid.
     *
     * @param int $gameid
     *
     * @return Highscore
     */
    public function setGameid($gameid)
    {
        $this->gameid = $gameid;

        return $this;
    }

    /**
     * Get gameid.
     *
     * @return int
     */
    public function getGameid()
    {
        return $this->gameid;
    }

    /**
     * Set userid.
     *
     * @param int $userid
     *
     * @return Highscore
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid.
     *
     * @return int
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set score.
     *
     * @param int $score
     *
     * @return Highscore
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score.
     *
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }
}
