<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

require __DIR__.'/config.php';
require __DIR__.'/core/DB.php';

$db = new DB($dbconfig);
$entity_manager = $db->getEntityManager();

return ConsoleRunner::createHelperSet($entity_manager);
?>
