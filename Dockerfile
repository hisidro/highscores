FROM ubuntu:18.04

ENV TZ=Australia/Brisbane
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y apache2

RUN apt-get install -y php libapache2-mod-php php-mysql

COPY dir.conf /etc/apache2/mods-enabled
COPY apache2.conf /etc/apache2

COPY .htaccess /var/www/html
COPY *.php /var/www/html/
RUN mkdir /var/www/html/api
COPY api/. /var/www/html/api/.
RUN mkdir /var/www/html/core
COPY core/. /var/www/html/core/.
RUN mkdir /var/www/html/vendor
COPY vendor/. /var/www/html/vendor/.
RUN mkdir /var/www/html/entities
COPY entities/. /var/www/html/entities/.
RUN mkdir /var/www/html/controllers
COPY controllers/. /var/www/html/controllers/.

RUN a2enmod rewrite

ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

