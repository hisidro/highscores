<?php

use \Firebase\JWT\JWT;

class JWToken {

    private $secretKey;
    private $token;
    private $encodedToken;
    private $decodedToken;
    private $exceptionMessage;

    public function __construct($token, $key) {
        $this->token = $token;
        $this->encodedToken = "";
        $this->decodedToken = "";
        $this->secretKey = $key;
        $this->exceptionMessage = "";
    }

    public function getExceptionMessage() {
        return $this->exceptionMessage;
    }

    public function getEncodedToken() {
        $this->encodedToken = JWT::encode($this->token, $this->secretKey);
        return $this->encodedToken;
    }

    public function getDecodedToken() {
        return $this->decodedToken;
    }

    public function isValidToken() {
        
        $result = FALSE;

        try {
            $this->decodedToken = JWT::decode($this->token, $this->secretKey, array('HS256'));
            $result = TRUE;
        } catch(Exception $e) {
            $this->exceptionMessage = $e->getMessage();
        }

        return $result;
    }

}


?>