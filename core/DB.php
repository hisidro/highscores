<?php

class DB {

    private $entityManager;

    function __construct($db){
        $doctrineConfiguration = Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
            $paths = [__DIR__ . '/../entities'],
            $isDevMode = true
        );
        $this->entityManager = Doctrine\ORM\EntityManager::create($db, $doctrineConfiguration);
    }

    function getEntityManager() {
        return $this->entityManager;
    }

    // not used now since we are using Doctrine
    function connect($db) {

        $host = $db['host'];
        $dbname = $db['dbname'];
        $uname = $db['user'];
        $pword = $db['password'];

        $connection_string = "mysql:host=" . $host .";dbname=" . $dbname;

        try{
            $conn = new PDO($connection_string, $uname, $pword);

            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            return $conn;
        } catch (PDOException $exception) {
            exit($exception->getMessage());
        }
    }
}

?>