<?php

class Responder {

    private $header_status = "";
    private $response = array();


    private $status_codes = [
        200 => ' 200 OK',
        201 => ' 201 Created.',
        401 => ' 401 Unauthorized.',
        404 => ' 404 Not Found.',
        405 => ' 405 Method not allowed.',
        500 => ' 500 Internal server error.'
    ];

    function __construct($status, $status_message, $payload) {

        $header_status_message = $this->status_codes[$status];

        $this->header_status = $_SERVER['SERVER_PROTOCOL'] . $header_status_message;
        $this->response = array("status"=>$status, "status_message"=>$status_message, "payload"=>$payload);
    }

    function sendResponse() {
        header($this->header_status);
        header('Content-Type: application/json');
        echo json_encode($this->response);
    }

}

?>