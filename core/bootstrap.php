<?php

require __DIR__.'/DB.php';
require __DIR__.'/Router.php';
require __DIR__.'/JWToken.php';
require __DIR__.'/Responder.php';
require __DIR__.'/Validator.php';
require __DIR__.'/../routes.php';
require __DIR__.'/../config.php';

require_once __DIR__."/../vendor/autoload.php";

# setup Doctrine
$db = new DB($dbconfig);
$entity_manager = $db->getEntityManager();

$router = new Router;
$router->setRoutes($routes);

if(array_key_exists('REQUEST_URI', $_SERVER)){

    $url = $_SERVER['REQUEST_URI'];

    if(strcmp($router->getFilename($url), "Not Found") == 0){
        $responder = new Responder(404, 'Endpoint not found.', array());
        $responder->sendResponse();
    } else {
        
        if(array_key_exists('Authorization', getallheaders())){
            $bearer = substr(getallheaders()['Authorization'], 7);
        } else {
            $bearer = '';
        }
        
        $request_method = $_SERVER['REQUEST_METHOD'];

        require __DIR__."/../api/".$router->getFilename($url);
    }
}

?>