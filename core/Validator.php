<?php

class Validator {

    private $response;
    private $url;
    private $bearerToken;
    private $secretKey;
    private $id;
    private $endpoint;
    private $sizePath;

    public function __construct($url, $bearer, $key) {
        $this->response = NULL;
        $this->url = $url;
        $this->bearerToken = $bearer;
        $this->secretKey = $key;

        $path = explode('/', parse_url($this->url)['path']);
        $this->sizePath = sizeof($path);
        $this->id = $path[3];
        if(is_numeric($this->id)) {
            $this->id = $this->id + 0;
        }
        $this->endpoint = $path[2];
    }

    public function getId() {
        return $this->id;
    }

    public function getEndpoint() {
        return $this->endpoint;
    }

    public function validate() {
        if($this->sizePath == 4 && (is_integer($this->id) || $this->id == NULL)) {
            if(isset($this->bearerToken) && !empty($this->bearerToken)) {       
                $jtoken = new JWToken($this->bearerToken, $this->secretKey);
                if(!$jtoken->isValidToken()) {
                    $this->response = new Responder(401, $jtoken->getExceptionMessage(), array());
                }      
            } else {
                $this->response = new Responder(401, 'No token passed, access denied.', array());
            }    
        } else {
            $this->response = new Responder(404, 'Endpoint not found.', array());
        }
        return $this->response;
    }

}


?>