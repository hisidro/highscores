<?php

require __DIR__."/../entities/Highscore.php";

class HighScoresController {

    private $entityManager;

    public function __construct($em) {
        $this->entityManager = $em;
    }

    public function writeScore($gameid, $userid, $score) {
        try {
            $this->entityManager->getConnection()->beginTransaction();
            $highscore = (new entities\Highscore())->setGameid($gameid)
                                                   ->setUserid($userid)
                                                   ->setScore($score);
            $this->entityManager->persist($highscore);
            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
            $status = 201;
            $status_message = "Successfully added score to minigame with id = " . $gameid . ".";
        } catch(Exception $ex) {
            $status = 500;
            $status_message = "Exception Thrown: " . $ex->getMessage();
        }

    return new Responder($status, $status_message, array());
    }

    public function getScores($gameid, $mode) {
        $results = array();
        $status = '';
        $status_message = '';
    
        if($gameid > 0) {
    
            $qb = $this->entityManager->createQueryBuilder();
            $qb->select('h')
               ->from('entities\Highscore', 'h')
               ->where('h.gameid = ' . $gameid)
               ->orderBy('h.score', 'DESC');
    
            if($mode == 'topten') {
                $qb->setMaxResults(10);
            }
    
            try {
                $query = $qb->getQuery();
                $results = $query->getArrayResult();
                $status = 200;
                $status_message = "Fetched results.";
            } catch(Exception $ex) {
                $status = 500;
                $status_message = "Exception Thrown: " . $ex->getMessage();
            }
        } else {
            $status = 500;
            $status_message = 'Invalid minigame id.';
        }
    
        return new Responder($status, $status_message, $results);
    }
}

?>