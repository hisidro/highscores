<?php

require __DIR__."/../entities/Minigame.php";

class MiniGamesController {

    private $entityManager;

    public function __construct($e) {
        $this->entityManager = $e;
    }

    public function getMinigamesList($gameid) {
        $results = array();
        $status = '';
        $status_message = '';
    
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('m')
           ->from('entities\Minigame', 'm');
    
        if($gameid > 0) {
            $qb->where("m.id = " . $gameid);
        }
    
        $query = $qb->getQuery();
    
        try {
            $results = $query->getArrayResult();
            $status = 200;
            $status_message = "Fetched results.";
        } catch(Exception $ex) {
            $results = array();
            $status = 500;
            $status_message = "Exception Thrown: " . $ex->getMessage();
        }
    
        return new Responder($status, $status_message, $results);
    }
    
    public function addMinigame($name) {
        $this->entityManager->getConnection()->beginTransaction();
        try {
            $minigame = (new entities\Minigame())->setName($name);
            $this->entityManager->persist($minigame);
            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
            $status = 201;
            $status_message = "Successfully added new minigame, '" . $name . "'";
        } catch(Exception $ex) {
            $this->entityManager->getConnection()->rollBack();
            $status = 500;
            $status_message = "Exception Thrown: " . $ex->getMessage();
        }

        return new Responder($status, $status_message, array());
    }

}

?>
