<?php

require __DIR__."/../entities/User.php";

class UsersController {

    private $entityManager;
    private $data;
    private $webtoken;
    
    public function __construct($em, $data, $wt) {
        $this->entityManager = $em;
        $this->data = $data;
        $this->webtoken = $wt;
    }

    public function checkEndpoint($url) {
        $path = explode('/', parse_url($url)['path']);
        
        if(sizeof($path) == 4 && empty($path[3])) {
            return NULL;
        } else {
            return new Responder(404, 'Endpoint not found.', array());
        }
    }

    public function getToken() {
        $status = '';
        $status_message = '';
        $jwt = '';
    
        if((array_key_exists('uname', $this->data)) && 
           (array_key_exists('email', $this->data)) &&
           (array_key_exists('pword', $this->data))) {
    
            $uname = htmlspecialchars(strip_tags($this->data['uname']));
            $email = htmlspecialchars(strip_tags($this->data['email']));
    
            $user = $this->entityManager->getRepository('entities\User')
                                        ->findOneBy(array('uname' => $uname, 'email' => $email));
    
            if(!is_null($user)) {
                //password was hashed using password_hash using Argon2i
                if(password_verify($this->data['pword'], $user->getPword())) {
                    $token = array(
                        'iss' => $this->webtoken['iss'],
                        'aud' => $this->webtoken['aud'],
                        'iat' => time(),
                        'exp' => time() + 15000,
                        'data' => array(
                            'uname' => $user->getUname(),
                            'email' => $user->getEmail() 
                        )
                    );
                    $status = 200;
                    $jtoken = new JWToken($token, $this->webtoken['secret_key']);
                    $jwt = $jtoken->getEncodedToken();
                    $status_message = "Token sent.";
                } else {
                    $status = 401;
                    $status_message = "Token not sent, incorrect password sent.";
                }
            } else {
                $status = 401;
                $status_message = "Token not sent, username and/or email not found.";
            }
        } else {
                $status = 500;
                $status_message = "Invalid input data.";
        }
    
        return new Responder($status, $status_message, $jwt);
    }

}

?>