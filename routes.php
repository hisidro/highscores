<?php

$routes = [
    'minigames' => 'minigames.php',
    'topten' => 'hiscores.php',
    'allscores' => 'hiscores.php',
    'highscores' => 'hiscores.php',
    'gettoken' => 'users.php'
];

?>