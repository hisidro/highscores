<?php

require __DIR__.'/../../core/Router.php';

use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase {

    private $router;
    private $routes = [
        'route1' => 'file1.php',
        'route2' => 'file2.php',
    ];
    private $url;

    public function setUp():void {
        $this->router = new Router();    
        $this->router->setRoutes($this->routes);
    }

    public function testGetFilenameSuccess() {
        $this->url = "https://www.test.com/api/route2";
        $this->assertEquals("file2.php", $this->router->getFilename($this->url));
    }

    public function testGetFilenameNotFound() {
        $this->url  = "https://www.test.com/api/route66";
        $this->assertEquals("Not Found", $this->router->getFilename($this->url));
    }
}

?>