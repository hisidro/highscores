<?php

require __DIR__."/../controllers/UsersController.php";

$data = json_decode(file_get_contents('php://input'), true);
$uc = new UsersController($entity_manager, $data, $webtoken);
$response = $uc->checkEndpoint($url);

if($response == NULL) {
    switch($request_method) {
        case 'POST':
            $response = $uc->getToken();
            break;
        default:
            $response = new Responder(405, 'Method not allowed.', array());
            break;
    }
}

$response->sendResponse();

?>