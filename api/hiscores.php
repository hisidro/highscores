<?php

require __DIR__."/../controllers/HighScoresController.php";

$hsc = new HighScoresController($entity_manager);
$validator = new Validator($url, $bearer, $webtoken['secret_key']);
$response = $validator->validate();

if($response == NULL) {            
    switch($request_method) {
        case 'GET':
            switch($validator->getEndpoint()) {
                case 'topten':
                    $response = $hsc->getScores($validator->getId(), 'topten');
                    break;
                case 'allscores':
                    $response = $hsc->getScores($validator->getId(), 'all');
                    break;
                case 'highscores':
                    $response = new Responder(405, 'Method not allowed.', array());
                    break;
                default:
                    $response = new Responder(404, 'Endpoint not found.', array());
                break;
            }
            break;
        case 'POST':
            if($validator->getEndpoint() == 'highscores') {
                $data = json_decode(file_get_contents('php://input'), true);

                if( (isset($data['gameid'])) &&
                    (isset($data['userid'])) &&
                    (isset($data['score']))
                ) {
                    $gameid = htmlspecialchars(strip_tags($data['gameid']));
                    $userid = htmlspecialchars(strip_tags($data['userid']));
                    $score = htmlspecialchars(strip_tags($data['score']));

                    $response = $hsc->writeScore($gameid, $userid, $score);
                } else {
                    $response = new Responder(500, "Invalid or incomplete input data.", array());
                }
            } else {
                $response = new Responder(404, 'Endpoint not found.', array());
            }
            break;
        default:
            $response = new Responder(405, 'Method not allowed.', array());
            break;
    }
}

$response->sendResponse();

?>