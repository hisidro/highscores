<?php

require __DIR__."/../controllers/MiniGamesController.php";

$mgc = new MinigamesController($entity_manager);
$validator = new Validator($url, $bearer, $webtoken['secret_key']);
$response = $validator->validate();

if($response == NULL) {
    switch($request_method) {
        case 'GET':
            $response = $mgc->getMinigamesList($validator->getId());
            break;
        case 'POST':
            $data = json_decode(file_get_contents('php://input'), true);
            if(isset($data['name'])) {
                $name = htmlspecialchars(strip_tags($data['name']));
                $response = $mgc->addMinigame($name);
            } else {
                $response = new Responder(500, "Invalid or incomplete input data", array());
            }
            break;
        default:
            $response = new Responder(405, 'Method not allowed.', array());
            break;
    }
}

$response->sendResponse();

?>